package solutions

object TermsAndTypes {
  def termsAndTypes(): Unit = {
    println(16.toHexString) //10
    println((0 to 10).contains(10)) //true
    println((0 until 10).contains(10)) //false
    println("foo".drop(1)) //oo
    println("bar".take(2)) //ba
  }
}

package solutions

object DefinitionsAndEvaluation {
    def square(x: Double): Double = x*x
    def circleArea(radius: Double): Double = 3.14159 * square(radius)
    def triangleArea(base: Double, height: Double): Double =
        base*height/2
}

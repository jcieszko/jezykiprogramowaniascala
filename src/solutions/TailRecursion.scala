package solutions

import scala.annotation.tailrec

object TailRecursion {

    //funkcja ktora nie jest tail recursive
    def factorial(n: Int): Int =
        if (n == 0) 1
        else factorial(n - 1) * n

    //funkcja ktora jest tail recursive
    def factorial_tr(n: Int): Int = {
        @tailrec
        def iter(x: Int, result: Int): Int =
            if (x == 0) result
            else iter(x - 1, result * x)

        iter(n,1)
    }

    //wytlumaczenie:
    //Regular Recursion: Wywoluje siebie a potem jeszcze cos robi z wynikiem. Zuzywa wiecej pamieci.
    //Tail Recursion: Wywoluje siebie i juz nic wiecej nie robi z wynikiem. Zuzywa malo pamieci, przypomina petle.

}

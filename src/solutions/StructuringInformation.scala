package solutions

object StructuringInformation {
    sealed trait Symbol
    case class Note(name: String, duration: String, octave: Int) extends Symbol
    case class Rest(duration: String) extends Symbol

    val c3: Note = Note("C", "quarter", 3)
    val symbol1: Symbol = Note("C", "quarter", 3)
    val symbol2: Symbol = Rest("whole")

    //pattern matching - ekstrakcja konktetnego pola ktore wystepuje w obu klasach dziedziczacych po symbol
    def symbolDuration(symbol: Symbol): String =
        symbol match {
            case Note(name, duration, octave) => duration
            case Rest(duration) => duration
        }

    sealed trait Duration
    case object Whole extends Duration
    case object Half extends Duration
    case object Quarter extends Duration

    def fractionOfWhole(duration: Duration): Double =
        duration match {
            case Whole => 1.0
            case Half => 0.5
            case Quarter => 0.25
        }
}

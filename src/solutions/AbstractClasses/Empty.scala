package solutions.AbstractClasses

class Empty extends IntSet{
    def contains(x: Int): Boolean = false
    def incl(x: Int): IntSet = new NonEmpty(x, new Empty, new Empty)
}

class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {
    def contains(x: Int): Boolean =
        if (x < elem) left contains x
        else if (x > elem) right contains x
        else true

    def incl(x: Int): IntSet =
        if (x < elem) new NonEmpty(elem, left incl x, right)
        else if (x > elem) new NonEmpty(elem, left, right incl x)
        else this
}

// W Scali klasy, obiekty i cechy (traits) moga dziedziczyc po JEDNEJ klasie, ale wielu cechach
// Slowo kluczowe with

trait Planar {
    def height: Int
    def width: Int
    def surface = height * width
}

//class Square extends Shape with Planar with Movable …
package solutions

import scala.annotation.tailrec
import scala.math.abs

object LexicalScopes {
    def squareRoot(x: Double): Double = {
        @tailrec
        def squareRootIterator(guess: Double): Double =
            if (isGoodEnough(guess, x)) guess
            else squareRootIterator(improve(guess, x))

        def isGoodEnough(guess: Double, x: Double): Boolean =
            abs(guess * guess - x) < 0.001

        def improve(guess: Double, x: Double): Double =
            (guess + x / guess) / 2

        squareRootIterator(1.0)
    }


}

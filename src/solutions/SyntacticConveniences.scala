package solutions

object SyntacticConveniences {

    // STRING INTERPOLATION
    // dodanie tego s na poczatku umozliwia poslugiwanie sie znakiem $ w celu wyswietlania podanych wartosci
    def greet(name: String): String =
        s"Hello, $name!"

    // TUPLES
    def pair(i: Int, s: String): (Int, String) = (i, s)
    val tupleTry: (String, Int) = ("imagine", 76)

    // FOR EXPRESSIONS
    def loopA(xs: List[Int]): List[Int] =
        for (x <- xs) yield x + 1

    def filterA(xs: List[Int]): List[Int] =
        for (x <- xs if x % 2 == 0) yield x
}

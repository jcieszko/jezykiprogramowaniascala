package solutions

import scala.annotation.tailrec

object ObjectOrientedProgramming {
    class Rational(x: Int, y: Int) {
        require(y > 0, "denominator must be positive")
        @tailrec
        private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
        val numer: Int = x / gcd(x, y)
        val denom: Int = y / gcd(x, y)

        // METODY
        def add(r: Rational) =
            new Rational(numer * r.denom + r.numer * denom, denom * r.denom)

        def + (r: Rational) =
            new Rational(numer * r.denom + r.numer * denom, denom * r.denom)

        def less(that: Rational): Boolean =
            numer * that.denom < that.numer * denom

        //EKWIWALENT:
//        def less(that: Rational) =
//            this.numer * that.denom < that.numer * this.denom

        def max(that: Rational): Rational =
            if (this.less(that)) that
            else this

        override def toString = s"$numer/$denom"

    }
    val x = new Rational(1, 2)
    val xNumer: Int = x.numer
    val xDenom: Int = x.denom

    // DODAWANIE ULAMKOW ZWYKLYCH
    // n1 / d1 + n2 / d2 = (n1 * d2 + n2 * d1) / (d1 * d2)
    // gdzie n1 i n2 to liczniki (numerators) a d1 i d2 to mianowniki (denominators)

    // DODAWANIE LICZB RZECZYWISTYCH W POSTACI UŁAMKÓW ZWYKŁYCH
    def addRational(r: Rational, s: Rational): Rational =
        new Rational(
            r.numer * s.denom + s.numer * r.denom,
            r.denom * s.denom) // na podstawie rownania 1

    def makeString(r: Rational) =
        s"${r.numer}/${r.denom}"

    val rationalA = new Rational(1, 2)
    val rationalB = new Rational(2, 3)
}

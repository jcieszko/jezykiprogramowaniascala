package solutions

import scala.::

object StandardLibrary {
    //Wszystkie listy sa skonstruowane z:
    // - pustej listy Nil
    // - operacji :: (cons)
    // x::xs daje nowa liste zaczynajaca sie od elementu x po ktorym nastepuja elementy z listy xs
    val fruit: List[String] = List("apples", "oranges", "pears")
    val fruit_a = "apples"::("oranges"::("pears"::Nil))

    val nums: List[Int] = List(1, 2, 3, 4)
    val nums_a = 1 :: (2 :: (3 :: (4 :: Nil)))
    val nums_b = 1 :: 2 :: 3 :: 4 :: Nil
    val nums_c = Nil.::(4).::(3).::(2).::(1)

    //przyklad 1: sortowanie list
    //chcemy ulozyc liste liczb w kolejnosci rosnacej
    //zeby posortowac liste List(7, 3, 9, 2) najpierw sortujemy jej ogon czyli List(3, 9, 2)
    //a potem wstawiamy 7 w odpowiednie miejsce. Wynik powinien wynosic List(2, 3, 7, 9)
    //taki sposob to SORTOWANIE INSERCYJNE

    def insertionSort(xs: List[Int]): List[Int] = xs match {
        case List() => List()
        case y :: ys => insert(y, insertionSort(ys))
    }
    // sprawdzenie czy x jest mniejsze lub rowne y
    val cond: (Int, Int) => Boolean =
        (x, y) => x <= y
    def insert(x: Int, xs: List[Int]): List[Int] = xs match {
        case List() => x :: Nil // Jeśli lista jest pusta, wstawiamy element na jej początek
        case y :: ys => // Jesli lista nie jest pusta
            if(cond(x, y)) x :: y :: ys // Jeśli x jest mniejsze lub równe y, wstawiamy x przed y
            else y :: insert(x, ys) // W przeciwnym razie kontynuujemy szukanie miejsca dla x w pozostałej części listy
    }

    //Typy opcjonalne
    //kiedy wynik moze albo nie istniec czyli byc typu None, albo istniec i byc np typu Double
//    def sqrt(x: Double): Option[Double] =
//        if (x < 0) None else Some()...
    //W skali mamy dwa typy opcjonalne: Some i None
    //Mapowanie typow opcjonalnych
    val optionalA: Option[Int] = Some(1)
    val optionalB: Option[Int] = optionalA.map(x => x + 1)
    val noneA: Option[Int] = None.map((x: Int) => x + 1)

}

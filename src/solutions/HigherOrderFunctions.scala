package solutions

import solutions.FunctionalLoops.factorial

object HigherOrderFunctions {
    def sumInts_a(a: Int, b: Int): Int =
        if (a > b) 0
        else a + sumInts(a + 1, b)

    def cube(x: Int): Int = x * x * x

    def sumCubes_a(a: Int, b: Int): Int =
        if (a > b) 0
        else cube(a) + sumCubes(a + 1, b)

    def factorial(n: Int): Int =
        if (n == 0) 1
        else factorial(n - 1) * n

    def sumFactorials_a(a: Int, b: Int): Int =
        if (a > b) 0 else factorial(a) + sumFactorials(a + 1, b)

    def sum(f: Int => Int, a: Int, b: Int): Int =
        if (a > b) 0
        else f(a) + sum(f, a + 1, b)

    def id(x: Int): Int = x

    def sumInts(a: Int, b: Int): Int = sum(id, a, b)
    def sumCubes(a: Int, b: Int): Int = sum(cube, a, b)
    def sumFactorials(a: Int, b: Int): Int = sum(factorial, a, b)

//    wniosek: zamiast osobnych funkcji do sumowania wynikow operacji na liczbach pomiedzy danymi liczbami
//      mozna napisac nadrzedna funkcje sumujaca, do ktorej bedziemy przekazywac funkcje jakie chcemy wykonac na
//      tych liczbach

    //funkcje bez nazwy to funkcje anonimowe, np:
    (x: Int) => x * x * x
    (x: Int, y: Int) => x + y

    //uzywajac funkcji anonimowych mozemy zapisac funkcje sumujace w jeszcze prostszy sposob:
    def sumIntsAnonymus(a: Int, b: Int): Int = sum(x => x, a, b)
    def sumCubesAnonymus(a: Int, b: Int): Int = sum(x => x * x * x, a, b)

}

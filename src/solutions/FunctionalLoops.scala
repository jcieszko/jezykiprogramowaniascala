package solutions

import scala.annotation.tailrec
import scala.math.abs

object FunctionalLoops {

    // pierwiastkowanie
    def squareRoot(x: Double): Double =
        squareRootIterator(guess = 1, x)

    @tailrec
    def squareRootIterator(guess: Double, x: Double): Double =
        if (isGoodEnough(guess, x)) guess
        else squareRootIterator(improve(guess, x), x)

    def isGoodEnough(guess: Double, x: Double): Boolean =
        abs(guess * guess - x) < 0.001

    def improve(guess: Double, x: Double): Double =
        (guess + x / guess) / 2


    //silnia
    def factorial(n: Int): Int =
        if (n == 0) 1
        else factorial(n-1) * n
}

import Lista1.Zadanie1.Triangle.heron
import Lista1.Zadanie2.Multiset.findIntersetion
import solutions.{DefinitionsAndEvaluation, FunctionalLoops, HigherOrderFunctions, LexicalScopes, ObjectOrientedProgramming, StandardLibrary, SyntacticConveniences, TailRecursion, TermsAndTypes}

object Main {
  def main(args: Array[String]): Unit = {
    // Zadanie 1
//    TermsAndTypes.termsAndTypes()

    // Zadanie 2
//    println(DefinitionsAndEvaluation.square(x = 3))
//    println(DefinitionsAndEvaluation.circleArea(radius = 4))
//    println(DefinitionsAndEvaluation.triangleArea(base = 3, height = 7))

    // Zadanie 3
//    println(FunctionalLoops.squareRoot(3))
//    println(FunctionalLoops.factorial(n = 5))

    // Zadanie 4
//    println(LexicalScopes.squareRoot(3))

    // Zadanie 5
//    println(TailRecursion.factorial(6))
//    println(TailRecursion.factorial_tr(6))

    // Zadanie 7 (spodziewamy sie pieciu wartosci true na wyjsciu)
//    println(HigherOrderFunctions.sumInts_a(1, 5) == HigherOrderFunctions.sumInts(1, 5))
//    println(HigherOrderFunctions.sumCubes_a(1, 5) == HigherOrderFunctions.sumCubes(1, 5))
//    println(HigherOrderFunctions.sumFactorials_a(1, 5) == HigherOrderFunctions.sumFactorials(1, 5))
//    println(HigherOrderFunctions.sumInts(1, 5) == HigherOrderFunctions.sumIntsAnonymus(1, 5))
//    println(HigherOrderFunctions.sumCubes(1, 5) == HigherOrderFunctions.sumCubesAnonymus(1, 5))

    // Zadanie 8
//    println(StandardLibrary.insertionSort(List(7, 3, 2, 9))) // oczekujemy List(2, 3, 7, 9)
//    // przykladowe operacje na listach
//    println(List(1, 2, 3).map(x => x + 1)) //List(2, 3, 4)
//    println(List(1, 2, 3).filter(x => x % 2 == 0)) //List(2)
//    val xs =
//      List(1, 2, 3).flatMap { x =>
//        List(x, 2 * x, 3 * x)
//      }
//    println(xs) // xs = List(1, 2, 3, 2, 4, 6, 3, 6, 9)
//
//    //typy opcjonalne, cwiczenia
//    println(StandardLibrary.optionalA) //Some(1)
//    println(StandardLibrary.optionalB) //Some(2)
//    println(StandardLibrary.noneA) //None

    // Zadanie 9
//    // STRINGS
//    println(SyntacticConveniences.greet("Scala")) // Hello, Scala!
//    println(SyntacticConveniences.greet("Scala".toUpperCase())) // Hello, SCALA!
//    println(SyntacticConveniences.pair(34, "class")) // (34,class)
//    // TUPLES
//    println(SyntacticConveniences.tupleTry._1) // imagine
//    println(SyntacticConveniences.tupleTry._2) // 76
//    // FOR EXPRESSIONS
//    println(SyntacticConveniences.loopA(List(1, 2, 3))) // List(2, 3, 4)
//    println(SyntacticConveniences.filterA(List(1, 2, 3))) //List(2)
  }

  // Zadanie 10
//  println(ObjectOrientedProgramming.xNumer) //1
//  println(ObjectOrientedProgramming.xDenom) //2
//  // DODAWANIE ULAMKOW
//  // 1/2 + 2/3 = 7/6
//  val rationalA: ObjectOrientedProgramming.Rational = ObjectOrientedProgramming.rationalA
//  val rationalB: ObjectOrientedProgramming.Rational = ObjectOrientedProgramming.rationalB
//  val result: ObjectOrientedProgramming.Rational = ObjectOrientedProgramming.addRational(rationalA, rationalB)
//  println(ObjectOrientedProgramming.makeString(result))
//
//  // DODAWANIE ULAMKOW ALE METODA ADD JEST ZAWARTA W CIELE KLASY RATIONAL
//  val rationalD: ObjectOrientedProgramming.Rational = new ObjectOrientedProgramming.Rational(1, 2)
//  val rationalE: ObjectOrientedProgramming.Rational = new ObjectOrientedProgramming.Rational(2, 3)
//  val result2: ObjectOrientedProgramming.Rational = rationalD.add(rationalE)
//  println(result2.toString) //7/6
//  val result3: ObjectOrientedProgramming.Rational = rationalD.+(rationalE)
//  println(result3.toString) //7/6

  // LISTA 1
  // ZADANIE 1
//  try {
//    println(heron(3, 4, 5)) // 6
//    println(heron(7, 10, 5)) // 16.2481
//    println(heron(1, 1, 2)) // wyjatek
//  } catch {
//    case e: IllegalArgumentException => println(e.getMessage)
//  }
  // ZADANIE 2
//  try {
//    println(findIntersetion(List(1, 2, 2, 3), List(2, 2, 4))) // List(2, 2)
//    println(findIntersetion(List(1, 2, 3), List(4, 5, 6))) // List()
//    println(findIntersetion(List(), List(1, 2, 3))) // wyjatek
//  } catch {
//    case e: IllegalArgumentException => println(e.getMessage)
//  }

  // RESZTE ZADAN Z LISTY MOZNA URUCHOMIC Z POZIOMU PLIKOW W FOLDERACH LISTAX.ZADANIEY
  // DOPIERO NA TYM ETAPIE ODKRYLAM ZE KAZDY OBIEKT MOZE MIEC SWOJA FUNKCJE MAIN
}
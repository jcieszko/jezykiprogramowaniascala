package Lista1.Zadanie5

object Collatz {
    /**
     * Funkcja obliczajaca kolejne wartosci funkcji Collatza az do momentu gdy osiagnieta zostanie wartosc 1
     * @param c0 zadany parametr
     * @return lista kolejnych wartosci funkcji Collatza
     */
    def collatz(c0: Int): List[Int] = {
        if (c0 <= 0) throw new IllegalArgumentException("c0 musi byc liczba naturalna (wieksza od 0)")

        /**
         * Własciwa funkcja rekurencyjna obliczajaca wartosci funkcji Collatza
         * @param n zadany parametr
         * @param temporaryList tymczasowa lista do ktorej dodwane sa kolejne wartosci
         * @return lista wartosci po osiagnieciu wartosci parametru n = 1
         */
        @annotation.tailrec
        def calculateCollatz(n: Int, temporaryList: List[Int]): List[Int] = {
            if (n == 1) temporaryList :+ n
            else if (n % 2 == 0) calculateCollatz(n / 2, temporaryList :+ n)
            else calculateCollatz(3 * n + 1, temporaryList :+ n)
        }
        calculateCollatz(c0, List())
    }

    def main(args: Array[String]): Unit = {
        val testingValues = List(4, 13, 18, 38, 99)
        testingValues.foreach { value =>
            val collatzSequence = collatz(value)
            val maxElement = collatzSequence.max
            val length = collatzSequence.length
            println(s"c0: $value, Max element: $maxElement, Length: $length, Sequence: $collatzSequence")
        }
    }

}

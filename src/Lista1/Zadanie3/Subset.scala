package Lista1.Zadanie3

/**
 * Obiekt zawierajacy funkcje pozwalajace na poprawne znalezienie wszytskich podzbiorow zadanego zbioru
 */
object Subset {
    /**
     * Funkcja sprawdzajaca czy zbior nie jest pusty
     * @param x zbior
     * @tparam A typ zmiennych w zbiorze, generyczny
     * @return true jezeli nie jest pusty, false jezeli jest
     */
    def isSetNotEmpty[A](x: Set[A]): Boolean = x.nonEmpty

    /**
     * Funkcja znajdujaca podzbiory podanego zbioru, jezeli nie jest on pusty
     * @param x zbior
     * @tparam A typ zmiennych w zbiorze, generyczny
     * @return zbior podzbiorow
     */
    def findSubsets[A](x: Set[A]): Set[Set[A]] = {
        if (!isSetNotEmpty(x)) {
            throw new IllegalArgumentException("Pusty zbior")
        }

        /**
         * Funkcja znajdujaca podzbiory
         * @param s zbior
         * @return podzbiory
         */
        def subsets(s: Set[A]): Set[Set[A]] = {
            if (s.isEmpty) Set(Set.empty)
            else {
                val elem = s.head
                val restSubsets = subsets(s.tail)
                restSubsets ++ restSubsets.map(_ + elem) //_ + elem oznacza dodanie elem do kazdego podzbioru z restSubsets
            }
        }
        subsets(x)
    }

    def main(args: Array[String]): Unit = {
        val set = Set('a', 'b', 'c', 'd')
        try {
            println(findSubsets(set))
            println(findSubsets(Set())) // wyjatek
        } catch {
            case e: IllegalArgumentException => println(e.getMessage)
        }
    }


}

package Lista1.Zadanie6

object DNA {
    // Mapy komplementarnosci nukleotydow
    val nucleotidesCompDNA: Map[Char, Char] = Map('A' -> 'T', 'T' -> 'A', 'G' -> 'C', 'C' -> 'G')
    val nucleotidesCompRNA: Map[Char, Char] = Map('A' -> 'U', 'T' -> 'A', 'G' -> 'C', 'C' -> 'G')

    // Mapa kodonow do aminokwasow
    val codonsToAminoacids: Map[String, String] =
        Map(
            // Alanine
            "GCU" -> "A", "GCC" -> "A", "GCA" -> "A", "GCG" -> "A",
            // Arginine
            "CGU" -> "R", "CGC" -> "R", "CGA" -> "R", "CGG" -> "R", "AGA" -> "R", "AGG" -> "R",
            // Asparagine
            "AAU" -> "N", "AAC" -> "N",
            // Aspartic acid
            "GAU" -> "D", "GAC" -> "D",
            // Cysteine
            "UGU" -> "C", "UGC" -> "C",
            // Glutamic acid
            "GAA" -> "E", "GAG" -> "E",
            // Glutamine
            "CAA" -> "Q", "CAG" -> "Q",
            // Glycine
            "GGU" -> "G", "GGC" -> "G", "GGA" -> "G", "GGG" -> "G",
            // Histidine
            "CAU" -> "H", "CAC" -> "H",
            // Isoleucine
            "AUU" -> "I", "AUC" -> "I", "AUA" -> "I",
            // Leucine
            "UUA" -> "L", "UUG" -> "L", "CUU" -> "L", "CUC" -> "L", "CUA" -> "L", "CUG" -> "L",
            // Lysine
            "AAA" -> "K", "AAG" -> "K",
            // Methionine
            "AUG" -> "M",
            // Phenylalanine
            "UUU" -> "F", "UUC" -> "F",
            // Proline
            "CCU" -> "P", "CCC" -> "P", "CCA" -> "P", "CCG" -> "P",
            // Serine
            "UCU" -> "S", "UCC" -> "S", "UCA" -> "S", "UCG" -> "S", "AGU" -> "S", "AGC" -> "S",
            // Threonine
            "ACU" -> "T", "ACC" -> "T", "ACA" -> "T", "ACG" -> "T",
            // Tryptophan
            "UGG" -> "W",
            // Tyrosine
            "UAU" -> "Y", "UAC" -> "Y",
            // Valine
            "GUU" -> "V", "GUC" -> "V", "GUA" -> "V", "GUG" -> "V",
            // Stop codons
//            "UAA" -> "Stop", "UAG" -> "Stop", "UGA" -> "Stop"
        )

    /**
     * Funkcja tworzaca nic matrycowa na podstawie nici kodujacej
     * @param codingDNA nic kodujaca
     * @return nic matrycowa
     */
    def complement(codingDNA: String): String = {
        codingDNA.map(nucleotide => nucleotidesCompDNA(nucleotide))
    }

    /**
     * Funkcja tworzaca nic RNA na podstawie nici matrycowej DNA
     * @param matrixDNA nic matrycowa
     * @return nic mRNA
     */
    def transcribe(matrixDNA: String): String = {
        matrixDNA.map(nucleotide => nucleotidesCompRNA(nucleotide))
    }

    /**
     * Funkcja tworzaca lancuch aminokwasow na podstawie matrycowego RNA
     * @param mRNA RNA
     * @return aminokwasy
     */
    def translate(mRNA: String): String = {
        val codons = mRNA.grouped(3).toList
        val aminoAcids = codons.map { codon =>
            // Sprawdzenie, czy dany klucz (w tym przypadku kodon) istnieje w mapie.
            // Jeśli klucz istnieje, zwraca przypisaną mu wartość.
            // Jeśli nie, zwraca wartość domyślną (tutaj pusty ciąg "")
            codonsToAminoacids.getOrElse(codon, "")
        }
        aminoAcids.mkString("")
    }

    def main(args: Array[String]): Unit = {
        // Testy dla funkcji complement
        assert(complement("ATCG") == "TAGC")
        assert(complement("GGCC") == "CCGG")
        println("Testy dla funkcji complement zaliczone")

        // Testy dla funkcji transkrybuj
        assert(transcribe("ATCG") == "UAGC")
        assert(transcribe("GGCC") == "CCGG")
        println("Testy dla funkcji transcribe zaliczone")

        // Testy dla funkcji transluj
        assert(translate("AUGUUUUAA") == "MF")
        assert(translate("AUGUAA") == "M")
        println("Testy dla funkcji translate zaliczone")
    }
}

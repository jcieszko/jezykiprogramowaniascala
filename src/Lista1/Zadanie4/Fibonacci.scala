package Lista1.Zadanie4

import scala.annotation.tailrec

/**
 * Obiekt zawierajacy funkcje pozwalajace nq generowanie n pierwszych elementow ciagu Fibonacciego
 */
object Fibonacci {
    /**
     * Funkcja generujaca n pierwszych elementow ciagu Fibonacciego metoda iteracyjna
     * @param n ilosc elementow ciagu do wyswietlenia
     * @return lista wartosci n elementow ciagu
     */
    def fibonacciIteration(n: Int): List[Int] = {
        if (n < 0) throw new IllegalArgumentException("n nie moze byc ujemne")
        else if (n == 0) List()
        else if (n == 1) List(0)
        else {
            var fib = List(0, 1)
            for (_ <- 2 until n) {
                val sum = fib.takeRight(2).sum
                fib = fib :+ sum
            }
            fib
        }
    }

    /**
     * Funkcja generujaca n pierwszych elementow ciagu Fibonacciego metoda rekursji
     * @param n ilosc elementow ciagu do wyswietlenia
     * @return lista wartosci n elementow ciagu
     */
    def fibonacciRecursive(n: Int): List[Int] = {
        if (n < 0) throw new IllegalArgumentException("n nie moze byc ujemne")
        else if (n == 0) List()
        else if (n == 1) List(0)
        else {
            /**
             * Funkcja generujaca n pierwszych elementow ciagu Fibonacciego metoda rekursji
             * po zadeklarowaniu warunkow poczatkowych
             * @param initialA wartosc dwa do tytlu od aktualnego elementu ciqgu
             * @param initialB wartosc o jeden do tylu od aktualnego elementu ciagu
             * @param counter licznik
             * @param temporaryList lista tymczasowa do ktorej dodajemy kolejne elementy az licznik nie bedzie rowny 0
             * @return
             */
            @tailrec
            def fibTemporary(initialA: Int, initialB: Int, counter: Int, temporaryList: List[Int]): List[Int] = {
                if (counter == 0) temporaryList
                else fibTemporary(initialB, initialA + initialB, counter - 1, temporaryList :+ initialB)
            }
            fibTemporary(0, 1, n-1, List(0))
        }
    }

    def main(args: Array[String]): Unit = {
        try {
//            println(fibonacciIteration(10)) // List(0, 1, 1, 2, 3, 5, 8, 13, 21, 34)
//            println(fibonacciIteration(-1)) // wyjatek
            println(fibonacciRecursive(5))
            println(fibonacciRecursive(-4))
        } catch {
            case e: IllegalArgumentException => println(e.getMessage)
        }
    }

}

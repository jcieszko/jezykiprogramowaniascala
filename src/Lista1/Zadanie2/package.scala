package Lista1.Zadanie2

/**
 * Obiekt zawierajacy funkcje do obliczania czesci wspolnej zbiorow
 */
object Multiset {
    /**
     * Funkcja sprawdzajaca czy zbior nie jest pusty
     * @param x zbior
     * @tparam A typ zmiennych w zbiorze, tutaj generyczny
     * @return true jezeli zbior nie jest pusty, false w przeciwnym wypadku
     */
    def isMultisetNotEmpty[A](x: List[A]): Boolean = x.nonEmpty

    /**
     * Funkcja znajdujaca czesc wspolna zbiorow jezeli podane zbiory nie sa puste
     * @param x zbior pierwszy
     * @param y zbior drugi
     * @tparam A generyczny typ zmiennej w zbiorach
     * @return zbior bedacy czescia wspolna obu podanych zbiorow
     */
    def findIntersetion[A](x: List[A], y: List[A]): List[A] = {
        if (!isMultisetNotEmpty(x) || !isMultisetNotEmpty(y)) {
            throw new IllegalArgumentException("Nieprawidlowe zbiory")
        }

        /**
         * Funkcja znajdujaca czesc wspolna zbiorow
         * @param xs zbior pierwszy
         * @param ys zbior drugi
         * @return zbior stanowiacy czesc wspolna lub zbior pusty
         */
        def intersect(xs: List[A], ys: List[A]): List[A] = xs match {
            case Nil => Nil
            case head :: tail =>
                if (ys.contains(head)) head :: intersect(tail, ys.diff(List(head)))
                else intersect(tail, ys)
        }

        intersect(x, y)
    }
}

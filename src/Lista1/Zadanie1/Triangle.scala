package Lista1.Zadanie1

import scala.math.sqrt

/**
 * Obiekt zawierajacy funkcje potrzebne do obliczenia pola trojkata za pomoca wzoru Herona
 */
object Triangle {
    /**
     * Funkcja sprawdzajaca czy z opdanych bokow da sie utworzyc trojkat
     * @param a bok pierwszy
     * @param b bok drugi
     * @param c bok trzeci
     * @return true jezeli wszytskie warunki utworzenia trojkata sa spelnione, w przeciwnym wypadku false
     */
    def isTrianglePossible(a: Double, b: Double, c: Double): Boolean = {
        a > 0 && b > 0 && c > 0 && a + b > c && a + c > b && b + c > a
    }

    /**
     * Funkcja obliczajaca pole trojkata za pomoca wzoru herona po sprawdzeniu warunkow poczatkowych
     * @param a bok pierwszy
     * @param b bok drugi
     * @param c bok trzeci
     * @return pole trojkata
     */
    def heron(a: Double, b: Double, c: Double): Double = {
        if (!isTrianglePossible(a, b, c)) {
            throw new IllegalArgumentException("Nieprawidlowe boki trojkata")
        }
        val p = (a + b + c) / 2
        sqrt(p * (p - a) * (p - b) * (p - c))
    }
}